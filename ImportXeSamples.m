function [Samplenames,SampleXeRel,SampleXeRelErr,SamplesAKX,SamplesAKXErr,SamplesNe20Xe132,SamplesNe20Xe132Err]=ImportXeSamples(importpath,RelTo,Rel,RelErr,WhichAKXEndmembers,LastRow) 

% Specify where the measurement error is found in the excel sheet. If its
% always the next column to the measurment column, ErrorColumn = 1
ErrorColumn=1;


%Rel is the endmember relative to which the samples are normalised
opts=detectImportOptions(importpath,'Sheet','xenon','VariableNamesRange','A1','DataRange', 'A2');

opts = setvartype(opts,'double');
opts = setvartype(opts,{'NasaCode','LabCode'},'char');

Xe=readtable(importpath,opts);
Xe(LastRow:end,:)=[];
Xe(contains(Xe.NasaCode,'ReEx'),:)=[];
Xe(contains(Xe.NasaCode,'Dil'),:)=[]; %Crop out Re-Ex and Dilutions
Xe(contains(Xe.NasaCode,'dil'),:)=[]; %Crop out Re-Ex and Dilutions
Xe(contains(Xe.LabCode,'ReEx'),:)=[];
Xe(contains(Xe.LabCode,'Dil'),:)=[];
loc=cellfun('isempty', Xe{:,'NasaCode'} );
Xe(loc,:)=[];
Samplenames=Xe.NasaCode;

Xe124132=(Xe.x124Xe_132XeX100);
Xe124132errIdx=find(strcmp(Xe.Properties.VariableNames,'x124Xe_132XeX100'))+ErrorColumn; %column where the error of measurement is located. Here, the+1 means it's the next column to the 'x124Xe_132XeX100'
Xe124132err=Xe{:,Xe124132errIdx};
Xe126132=(Xe.x126Xe_132XeX100);
Xe126132errIdx=find(strcmp(Xe.Properties.VariableNames,'x126Xe_132XeX100'))+ErrorColumn; %column where the error of measurement is located. Here, the+1 means it's the next column to the 'x124Xe_132XeX100'
Xe126132err=Xe{:,Xe126132errIdx};
Xe128132=(Xe.x128Xe_132XeX100);
Xe128132errIdx=find(strcmp(Xe.Properties.VariableNames,'x128Xe_132XeX100'))+ErrorColumn; %column where the error of measurement is located. Here, the+1 means it's the next column to the 'x124Xe_132XeX100'
Xe128132err=Xe{:,Xe128132errIdx};
Xe130132=(Xe.x130Xe_132XeX100);
Xe130132errIdx=find(strcmp(Xe.Properties.VariableNames,'x130Xe_132XeX100'))+ErrorColumn; %column where the error of measurement is located. Here, the+1 means it's the next column to the 'x124Xe_132XeX100'
Xe130132err=Xe{:,Xe130132errIdx};
Xe131132=(Xe.x131Xe_132XeX100);
Xe131132errIdx=find(strcmp(Xe.Properties.VariableNames,'x131Xe_132XeX100'))+ErrorColumn; %column where the error of measurement is located. Here, the+1 means it's the next column to the 'x124Xe_132XeX100'
Xe131132err=Xe{:,Xe131132errIdx};
Xe134132=(Xe.x134Xe_132XeX100);
Xe134132errIdx=find(strcmp(Xe.Properties.VariableNames,'x134Xe_132XeX100'))+ErrorColumn; %column where the error of measurement is located. Here, the+1 means it's the next column to the 'x124Xe_132XeX100'
Xe134132err=Xe{:,Xe134132errIdx};
Xe136132=(Xe.x136Xe_132XeX100);
Xe136132errIdx=find(strcmp(Xe.Properties.VariableNames,'x136Xe_132XeX100'))+ErrorColumn; %column where the error of measurement is located. Here, the+1 means it's the next column to the 'x124Xe_132XeX100'
Xe136132err=Xe{:,Xe136132errIdx};
if any(size(WhichAKXEndmembers))
Ar36Xe132=Xe.x36Ar_132Xe;
Ar36Xe132err=Xe.x_57;
Kr84Xe132=Xe.x84Kr_132Xe;
Kr84Xe132err=Xe.x_52;
SamplesAKX=[Ar36Xe132 Kr84Xe132];
SamplesAKX(:,end+1)=ones(1,size(SamplesAKX,1));
SamplesAKXErr=[Ar36Xe132err Kr84Xe132err];

else
SamplesAKX=zeros(size(Xe136132,1),3);
SamplesAKXErr=zeros(size(Xe136132,1),3);  
end
SamplesXe=[Xe124132 Xe126132 Xe128132 Xe130132 Xe131132 Xe134132 Xe136132];
SampleXeerr=[Xe124132err Xe126132err Xe128132err Xe130132err Xe131132err Xe134132err Xe136132err];

SamplesXe=SamplesXe./100;
SampleXeerr=SampleXeerr./100;
SampleXeerr(find(isnan(SamplesXe(:,1)),1,'first'):end,:)=[]; 
SamplesAKX(find(isnan(SamplesXe(:,1)),1,'first'):end,:)=[]; 
SamplesAKXErr(find(isnan(SamplesXe(:,1)),1,'first'):end,:)=[]; 
Samplenames(find(isnan(SamplesXe(:,1)),1,'first'):end,:)=[]; 
SamplesXe(find(isnan(SamplesXe(:,1)),1,'first'):end,:)=[]; 


for i=1:size(SamplesXe,2)
    for j=1:size(SamplesXe,1)
        if isempty(RelTo)
            SampleXeRel(j,i)=SamplesXe(j,i);
           SampleXeRelErr(j,i)=SampleXeerr(j,i);
        else
        SampleXeRelErr(j,i)=sqrt(((SampleXeerr(j,i)./SamplesXe(j,i)).^2)+(RelErr(i)./Rel(i)).^2)*SamplesXe(j,i);
        SampleXeRel(j,i)=SamplesXe(j,i)/Rel(i);
        end
    end
end

SampleXeRel(:,end+1)=ones(size(SampleXeRel,1),1);
SampleXeRelErr(:,end+1)=zeros(size(SampleXeRel,1),1);
  
end
