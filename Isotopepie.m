function Isotopepie(GroupName,SampleNames,Endnames,NrOfSamples,Isotopes,s)

figure('Name',GroupName)

for i=1:NrOfSamples
subplot(2,NrOfSamples,i)
name=char(SampleNames{i});
m=Isotopes.(name);
l=m(1:s);
n=l(l>1);
p=pie(n,Endnames(l>1));
title(horzcat('Xe',name));

subplot(2,NrOfSamples,i+NrOfSamples)
l=m(s+1:end);
n=l(l>1);
EndnamesNe=Endnames(s+1:end);
p=pie(n,EndnamesNe(l>1));
title(horzcat('Ne',name))
end
end