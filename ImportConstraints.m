function [EndmemberNeXe,EndmemberNeXeErr]=ImportConstraints(WhichNeEndmembers,WhichXeEndmembers)
%All ratios are stated as
%20Ne/132Xe
%You can change endmember isotopic ratios here.
%%
WhichNeErr=strcat(WhichNeEndmembers,'Err'); 
WhichXeErr=strcat(WhichXeEndmembers,'Err'); 
%% 
Q=3.1;  %Ott 2002
QErr=0.7;

P3=80;
P3Err=53;

HL=485;
HLErr=26;
G=138;
GErr=19;
N=826;
NErr=117;
Ureilite=1.41;
UreiliteErr=0.2;
subsolar=7.59;
subsolarErr=1.07;

VErr=116.4;
V=216.7;

SW=6141;
SWErr=2383; %based on sample LAP02224 from Patrizia Will(2019)
W=443.75;


T=table(HL,Q,G,SW,subsolar,P3,N,V);
Terr=table(HLErr,QErr,GErr,SWErr,subsolarErr,P3Err,NErr,VErr);
idx=find(contains(T.Properties.VariableNames,WhichNeEndmembers)...
    &contains(T.Properties.VariableNames,WhichXeEndmembers));

EndmemberNeXe=T(:,idx);
EndmemberNeXeErr=Terr(:,idx);

end
