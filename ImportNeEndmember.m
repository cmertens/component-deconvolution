function [EndmemberNeTable,EndmemberNeErrTable]=ImportNeEndmember(WhichEndmembers)
%All ratios are stated as
% Endmember=[20/22Ne
%            21/22Ne];
%     You can change endmember isotopic ratios here.
%%
WhichErr=strcat(WhichEndmembers,'Err'); 
%%
Air=[9.8;0.029];
AirErr=[0;0];
P3=[8.91;0.029];%from Ott 2002
P3Err=[0.057;0.001];
SW=[13.6;0.0326];%Wieler 2002
SWErr=[0.4;0.0010];
HL=[8.5;0.036];%from Ott 2002
HLErr=[0.057;0.001];
Q1=[10.67;0.0294];%from Ott 2002
Q1Err=[0.02;0.001];
Q2=[10.11;0.0294];%from Ott 2002
Q2Err=[0.02;0.001];
Q=[mean([Q1(1) Q2(1)]) 0.0294]';
QErr=[sqrt((Q1Err(1)^2)+(Q2Err(1)^2));0.001];

G=[0.065 0.00059]'; %Ott2014 %Equivalent to Ne-E
GErr=[0;0]; %Not true; need to re-check
R=[0.01;0.0001]; 
Cosmogenic=[0.472 0.875]';
CosmogenicErr=[0.462;0.705];
W=[10.3 0.035]'; %Krietsch et al 2020
V=[9.08 0.0503 ]'; %Krietsch et al 2020, mean(step(1-6)
VErr=[0.1007 ;0.0092 ];
subsolar=[11.65 0]';%from Ott 2002


T=table(Air,HL,Q,G,R,Cosmogenic,V,W,SW,subsolar,P3);
Terr=table(AirErr,VErr,HLErr,QErr,GErr,CosmogenicErr,SWErr,P3Err);

EndmemberNeTable=T(:,WhichEndmembers);
EndmemberNeErrTable=Terr(:,WhichErr);
EndmemberNeTable(end+1,:)=num2cell(ones(1,size(EndmemberNeTable,2)));
EndmemberNeErrTable(end+1,:)=num2cell(zeros(1,size(EndmemberNeTable,2)));

end
