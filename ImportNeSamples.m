function [SamplesNe,SamplesNeErr]=ImportNeSamples(importpath,LastRow) %Rel is the endmember relative to which the samples are normalised
opts=detectImportOptions(importpath,'Sheet','neon','VariableNamesRange','A1','DataRange', 'A2');
opts = setvartype(opts,'double');
opts = setvartype(opts,{'NasaCode','LabCode'},'char');

% Specify where the measurement error is found in the excel sheet. If its
% always the next column to the measurment column, ErrorColumn = 1
ErrorColumn=1;

Ne=readtable(importpath,opts);
Ne(LastRow:end,:)=[];
Ne(contains(Ne.NasaCode,'ReEx'),:)=[];
Ne(contains(Ne.NasaCode,'Dil'),:)=[];
Ne(contains(Ne.NasaCode,'dil'),:)=[];
Ne(contains(Ne.LabCode,'ReEx'),:)=[];
Ne(contains(Ne.LabCode,'Dil'),:)=[];
loc=cellfun('isempty', Ne{:,'NasaCode'} );
Ne(loc,:)=[];

Ne2022=(Ne.x20Ne_22Ne);
Ne2022errIdx=find(strcmp(Ne.Properties.VariableNames,'x20Ne_22Ne'))+ErrorColumn; %column where the error of measurement is located. Here, the+1 means it's the next column to the 'x124Xe_132XeX100'
Ne2022Err=Ne{:,Ne2022errIdx};
Ne2122=Ne.x21Ne_22Ne;
Ne2122errIdx=find(strcmp(Ne.Properties.VariableNames,'x21Ne_22Ne'))+ErrorColumn; %column where the error of measurement is located. Here, the+1 means it's the next column to the 'x124Xe_132XeX100'
Ne2122Err=Ne{:,Ne2122errIdx};
SamplesNe=[Ne2022 Ne2122];
SamplesNe(:,end+1)=ones(1,size(SamplesNe,1));
SamplesNeErr=[Ne2022Err Ne2122Err];
SamplesNeErr(:,end+1)=zeros(1,size(SamplesNe,1));
SamplesNeErr(find(isnan(SamplesNe(:,1)),1,'first'):end,:)=[]; 
SamplesNe(find(isnan(SamplesNe(:,1)),1,'first'):end,:)=[]; 
end