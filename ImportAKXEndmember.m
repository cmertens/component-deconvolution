function [EndmemberAKX,EndmemberAKXErr]=ImportAKXEndmember(WhichEndmembers)
%All ratios are stated as
% Endmember=[20/22Ne
%            21/22Ne];
%     You can change endmember isotopic ratios here.
%%


WhichErr=strcat(WhichEndmembers,'Err'); 
%%
%% Define Ar/Xe and Kr/Xe endmembers
Q=[76 0.81]'; %According to Ott(2014)
QErr=[7 0.05]';

P3=[400 2.7]';
P3Err=[25 0.4]';

HL=[50 0.48]';
HLErr=[20 0.04]';
G=[3.55 0.465]'; %this is Ne-E and Xe-S
GErr=[0.5 0.066]';
N=[79 1.468]';
NErr=[11 0.208]';
subsolar=[2660 5.86]';
subsolarErr=[376 0.84]';
SW=[5.45E104  22.10]'; 
SWErr=[0 0]';

T=table(Q,P3,HL,G,N,subsolar,SW);
Terr=table(QErr,P3Err,HLErr,GErr,NErr,subsolarErr);

EndmemberAKXTable=T(:,WhichEndmembers);
EndmemberAKXErrTable=Terr(:,WhichErr);

EndmemberAKX=EndmemberAKXTable.Variables;
EndmemberAKXErr=EndmemberAKXErrTable.Variables;

EndmemberAKX(end+1,:)=ones(size(EndmemberAKX,2),1); %For constraint that all endmember proportions should sum to 1
EndmemberAKXErr(end+1,:)=zeros(size(EndmemberAKX,2),1); %For constraint that all endmember proportions should sum to 1
end
