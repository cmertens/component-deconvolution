function [EndmemberXeTable,EndmemberXeErrTable,Rel,RelErr]=ImportXeEndmember(WhichEndmembers,RelTo)
%All ratios are stated as
% Endmember=[124/132Xe
%     126/132Xe
%     128/132Xe
%     130/132Xe
%     131/132Xe
%     134/132Xe
%     136/132Xe];
%     You can change endmember isotopic ratios here.
%%
WhichErr=strcat(WhichEndmembers,'Err'); 
%%
Air=[0.003537 %Basford (1973)
    0.0033
    0.07136
    0.15136
    0.789
    0.3879
    0.3294 ];

AirErr=[0.000011
0.000017
0.00009
0.00012
0.0011
0.0006
0.0004];

HL=[0.00839 %Huss&Lewis(1994), recalculated using error-weighed regression
    0.00564
    0.0905
    0.1542
    0.8457
    0.6490
    0.7173];

HLErr=[0.00009 
    0.00008
    0.0006
    0.0003
    0.0013
    0.0342
    0.0380];

Q=[0.00455 %Busemann et al 2000
    0.00406
    0.0822
    0.1619
    0.8185
    0.3780
    0.3164
    ];

QErr=[0.00002
    0.000002
    0.0002
    0.0003
    0.0009
    0.0011
    0.0008];

P6=[0.00438
    0.00444
    0.089
    0.166
    0.8214
    0.3291
    0.31];

P6Err=[0.00025 %need to check, added same error for 136 as 134, is not true!
0.00028
0.02
0.011
0.047
0.05
0.05];

P6Exotic= [0.00687 %Gilmor et al 2005
    0.00521
    0.0899
    0.1589
    0.8355
    0.5180
    0.5500];
P6ExoticErr= [0.00008 %not true need to re-echeck!
    0.00008
    0.0005
    0.0003
    0.0013
    0.0013
    0.0013];

SW=[0.00492  %Ott 2014
    0.00417
    0.0842
    0.1649
    0.8263
    0.3692
    0.3003];

SWErr=[0.00007
    0.00009
    0.0003
    0.0004
    0.0012
    0.0007
    0.0006];

G=  [1e-9 %equivalent to Xe-S
    0.00033
    0.2159
    0.4826
    0.186
    0.0222
    0.0034];
GErr=[0.00034 %Need to check, taken from Huss&Lewis??136Err was assumed as 134Err
0.00019
0.0023
0.0042
0.012
0.0053
0.0053];

P3=[0.00451
    0.00404
    0.0806
    0.1591
    0.8232
    0.377
    0.31];
P3Err=[0.00006
0.00004
0.0002
0.0002
0.001
0.1
0.1];


subsolar=[0.00490
    0.00432
    0.0843
    0.1649
    0.8301
    0.3765
    0.3095];

subsolarErr=[0.00016
    0.00014
    0.0008
    0.001
    0.0034
    0.0025
    0.0020];
    

N=[0.00470 %Ott 2002
    0.00357
    0.0785
    0.1603
    0.8109
    0.4183
    0.4006];

NErr=[0.00013
0.00018
0.0011
0.0013
0.013
0.0059
0.0032];

W=[0.0039 %Krietsch 2020
    0.0036
    0.0742
    0.1534
    0.7969
    0.3888
    0.3339];

WErr=[0.0001 0.0001 0.0008 0.0016 0.0083 0.0041 0.0035]';

    

V=[ 0.0047%Krietsch 2020, mean(step 1-6)
    0.0042
    0.0816
    0.1620
    0.8211
    0.3852
    0.3362];
VErr=[ 0.0157    0.0278 1.1467    0.1774    0.3889    0.5100    1.5214]';%Krietsch 2020, std(step 1-6)

Pu244fiss=[1e-9 %from Gilmour(2018), supposed to be from Ozima&Podosek        
    1e-9         
    1e-9        
    1e-9    
    0.2780        
    1.0610    
    1.1299];
Pu244fissErr=[1e-9
    1e-9
    1e-9
    1e-9
    0.0074
    0.0318
    0.0339];

SpallBa=[0.5173  %This is cosmogenicSpallation on Ba Hohenberg et al. 1981
    0.9497 
    1.5394    
    1.1852    
    2.6591        
    0.0655
     1e-9]; 

SpallBaErr=[0.1707
    0.3134
    0.5088
    0.3928
    1.3888
    0.0016
    0];
    
SpallREE= [12.8333  %spallation on REE Hohenberg et al. 1981
    16.6667 
    20.3333    
    0.6667   
    27.3333             
    1e-9 
    1e-9];

SpallREEErr=[0.9343
    1.1667
    1.5485
    0.0760
    6.8333
0
0];
    

U238fiss=[1e-9  % from Gilmour 2018, originally from Ragettli et al (1994)       
    1e-9        
    1e-9         
    1e-9   
    0.1438       
    1.4350    
    1.2077]; 

U238fissErr=[1e-9 
    1e-9 
    1e-9 
   1e-9 
    0.0002
    0.0020
0.0012];
       
U235fiss=[ 1e-9     %from Gilmour(2018), supposed to be from Ragettli et al (1994)     
    1e-9         
    1e-9          
    1e-9    
    0.6756       
    1.8304    
    1.4881];

U235fissErr=[0
    0
    0
    0
    0.0057
    0.0198
    0.0089];

T=table(Air,HL,Q,G,SpallBa,V,W,SW,subsolar,SpallREE,P6,P6Exotic,U238fiss,Pu244fiss,U235fiss,P3,N);
Terr=table(AirErr,HLErr,QErr,GErr,SpallBaErr,VErr,WErr,SWErr,subsolarErr,SpallREEErr,P6Err,P6ExoticErr,U238fissErr,Pu244fissErr,U235fissErr,P3Err,NErr);

EndmemberXeTable=T(:,WhichEndmembers);
EndmemberXeErrTable=Terr(:,WhichErr);

EndmemberXe=EndmemberXeTable.Variables;
EndmemberXeErr=EndmemberXeErrTable.Variables;
% Divide all isotopic ratios by specified endmember.Calculate errors
if RelTo
Rel=T(:,RelTo).Variables;
RelErr=Terr(:,horzcat(RelTo,'Err')).Variables;

 for i=1:size(EndmemberXe,1)
     for j=1:size(EndmemberXe,2)
     EndmemberXe(i,j)=EndmemberXe(i,j)/Rel(i);
     EndmemberXeErr(i,j)=EndmemberXe(i,j)*sqrt(((EndmemberXeErr(i,j)/EndmemberXe(i,j))^2)+(RelErr(i)/Rel(i))^2);
     end
 end

EndmemberXeTable.Variables=EndmemberXe;
EndmemberXeTable(end+1,:)=num2cell(ones(1,size(EndmemberXe,2))); %For constraint that all endmember proportions should sum to 1
EndmemberXeErrTable.Variables=EndmemberXeErr;
EndmemberXeErrTable(end+1,:)=num2cell(zeros(1,size(EndmemberXe,2))); %For constraint that all endmember proportions should sum to 1
else
Rel=[];
RelErr=[];
EndmemberXeTable(end+1,:)=num2cell(ones(1,size(EndmemberXe,2))); %For constraint that all endmember proportions should sum to 1
EndmemberXeErrTable(end+1,:)=num2cell(zeros(1,size(EndmemberXe,2))); %For constraint that all endmember proportions should sum to 1
end
