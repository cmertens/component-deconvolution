clear variables;close all;clc;
%%
%This code calculates endmember proportions of Xe and Ne isotopic ratio measurements.
%We treat this as a least squares linear regression, minimizing the objective
%function f(c)=Ec-R, where E is the endmember matrix, R are the measured
%isotopic ratios and c is a vector containing contributions from chosen
%endmembers.
%% EXCEL FILE SET-UP
%1) You need to make sure that in the column "Nasa Code" all ReEx measurements
%contain a "ReEx" in the name, and all dilutions a "Dil", otherwise the
%code cannot recognize these entries.
%2) The code cannot handle NaNs, therefore make sure that all isotopic ratios
%are given for each sample. Some ratios can be omitted in the excel sheet
%because their error is too high. Either omit these samples or rise the
%error extrusion level.
% 3) In the "xenon" sheet, there should be colums containing the 36Ar/132Xe
% and 84Kr/132Xe if you want to use them to constrain endmember
% constributions.
%% INPUTS-----------------------------------------------------------------
%set inputs to either true or false
%-------------------------------------------------------------------------
plotvalidation=false; %if true, for each sample the isotopic ratios calculated
%from the regression are plotted against the originally measured ratio to asses
%how good the model fits the measured ratios

plotpie=false; %do you want to plot a piechart to see how much each endmember contributes to the mixture?

constraints=false; %do you want to introduce elemental constraints between Ne and Xe endmembers?

MonteCarlo=true; %if you want to use Monte Carlo. If not, put to false. If you select Monte Carlo, the output Isotope table will be the mean of all simulation runs, and the error 
                        % is the maximum of 5 and 95 percentile. 
MonteCarloRuns=10; %no. of simulations you want to run.
% Specify Importpath
importpath='C:\Users\cmertens\Documents\Noble Gases\NA03NA01.xlsx'; %path and name of the excel sheet lies
Savepath='C:\Users\cmertens\Documents\Noble Gases\Results.xlsx';

%% CHOOSE ENDMEMBER -----------------------------------------------------------------------------------

% Choose Xe Endmembers
%Possible Endmembers: 'Air','HL','Q','G','SpallBa','V','W','SW','subsolar','SpallREE','P6','P6Exotic','U238fiss','Pu244fiss','U235fiss','P3','N'

WhichXeEndmembers={'Air','HL','Q'};
RelTo='Q'; %Specify relative to which endmember you want your isotopic ratios,e.g.'Q'. If non, RelTo=[];

% Choose Ne Endmembers
%Choose between
%'Air','HL','Q','G','R','Cosmogenic','V','W','SW','subsolar','P3'
WhichNeEndmembers={'Q'};

% Choose 36Ar/132Xe and 84Kr/132Xe Endmembers
WhichAKXEndmembers={};

%% SET Additional Data options
LastRow=38; %Last Row in spreadsheet from which Data should be read in 
%% _________________________________________________________________________
%function importing the specified endmembers.
%To specify isotopic ratios, go to respective function
[EndmemberXe,EndmemberXeErr,Rel,RelErr]=ImportXeEndmember(WhichXeEndmembers,RelTo);
[EndmemberNe,EndmemberNeErr]=ImportNeEndmember(WhichNeEndmembers);
[EndmemberAKX,EndmemberAKXErr]=ImportAKXEndmember(WhichAKXEndmembers);


%% ____________________________________________________________________________________________________________________________________________
% WITH Monte Carlo
% ______________________________________________________________________________________________________________________________________________
 
if MonteCarlo  
 %Pre-assign cell array for monteCarlo array
 MonteSim=cell(1,MonteCarloRuns);
 for monterun=1:MonteCarloRuns
 
     
%% Picking a random value for the endmember matrix within error of the endmember
EndmemberXeIntervalmin = [EndmemberXe.Variables-EndmemberXeErr.Variables];
EndmemberXeIntervalmax = [EndmemberXe.Variables+EndmemberXeErr.Variables];
EndmemberXe.Variables = (EndmemberXeIntervalmax-EndmemberXeIntervalmin).*rand(1,1) + EndmemberXeIntervalmin;

EndmemberNeIntervalmin = [EndmemberNe.Variables-EndmemberNeErr.Variables];
EndmemberNeIntervalmax = [EndmemberNe.Variables+EndmemberNeErr.Variables];
EndmemberNe.Variables = (EndmemberNeIntervalmax-EndmemberNeIntervalmin).*rand(1,1) + EndmemberNeIntervalmin;
if EndmemberAKX
EndmemberAKXIntervalmin = [EndmemberAKX-EndmemberAKXErr];
EndmemberAKXIntervalmax = [EndmemberAKX+EndmemberAKXErr];
EndmemberAKX = (EndmemberAKXIntervalmax-EndmemberAKXIntervalmin).*rand(1,1) + EndmemberAKXIntervalmin;
end

%% Building endmember matrix from the specified endmembers

EndmemberXezero=[EndmemberXe.Variables;zeros(size(EndmemberNe,1)+size(EndmemberAKX,1),size(EndmemberXe,2))]; %adding zeros
EndmemberNezero=[zeros(size(EndmemberXe,1),size(EndmemberNe.Variables,2));EndmemberNe.Variables;zeros(size(EndmemberAKX,1),size(EndmemberNe.Variables,2))];
EndmemberAKXzero=[zeros(size(EndmemberXe,1)+size(EndmemberNe.Variables,1),size(EndmemberAKX,2));EndmemberAKX];
Endmember=[EndmemberXezero EndmemberNezero EndmemberAKXzero];
Endnames=[strcat(WhichXeEndmembers,'Xe') strcat(WhichNeEndmembers,'Ne') strcat(WhichAKXEndmembers,'AKX')];


%% Import samples
%Xenon
[Samplenames,SamplesXeRel,SamplesXeRelErr, SamplesAKX, SamplesAKXErr]...
    =ImportXeSamples(importpath,RelTo,Rel,RelErr,WhichAKXEndmembers,LastRow);
% pick a random number for sample value within sample threshhold
SamplesXeRelIntervalmin = [SamplesXeRel-SamplesXeRelErr];
SamplesXeRelIntervalmax = [SamplesXeRel+SamplesXeRelErr];
SamplesXeRel = (SamplesXeRelIntervalmax-SamplesXeRelIntervalmin).*rand(1,1) + SamplesXeRelIntervalmin;
%Neon
[SamplesNe,SamplesNeErr]=ImportNeSamples(importpath,LastRow);
% pick a random number for sample value within sample threshhold
SamplesNeIntervalmin = [SamplesNe-SamplesNeErr];
SamplesNeIntervalmax = [SamplesNe+SamplesNeErr];
SamplesNe = (SamplesNeIntervalmax-SamplesNeIntervalmin).*rand(1,1) + SamplesNeIntervalmin;

if any(size(SamplesAKX))
    % pick a random number for sample value within sample threshhold
    SamplesAKXRelIntervalmin = [SamplesAKX-SamplesAKXErr];
    SamplesAKXRelIntervalmax = [SamplesAKX+SamplesAKXErr];
    SamplesAKX = (SamplesAKXRelIntervalmax-SamplesAKXRelIntervalmin).*rand(1,1) + SamplesAKXRelIntervalmin;
    Samples=[SamplesXeRel SamplesNe SamplesAKX];

else
    Samples=[SamplesXeRel SamplesNe];
end

%% COMPUTE CONSTRAINTS%%%------------------------------------------------------------------------------------------------------
%Import Element proportions
if constraints
    [EndmemberNeXe,EndmemberNeXeErr]=ImportConstraints(WhichNeEndmembers,WhichXeEndmembers);
    %Set up inequality matrix
    Names=EndmemberNeXe.Properties.VariableNames;
    Variables=EndmemberNeXe.Variables;
    VariablesErr=EndmemberNeXeErr.Variables;
    for i=1:size(EndmemberNeXe,2)
        
        Xeidx=(contains(EndmemberXe.Properties.VariableNames,Names(i)));
        Xemin=Xeidx*(Variables(i)-VariablesErr(i));
        Xemax=Xeidx*(Variables(i)+VariablesErr(i));
        
        Neidx=(contains(EndmemberNe.Properties.VariableNames,Names(i)));
        A(i+i-1,:)=[Xemin -Neidx zeros(1,size(EndmemberAKX,2))];
        A(i+i,:)=[-Xemax Neidx zeros(1,size(EndmemberAKX,2))];
    end
    b=zeros(size(A,1),1);
else
    A=[];
    b=[];
end
Aeq=[];
beq=[];

%% Minimization

for i=1:size(Samples,1)
    [xf(:,i),fval(:,i),exitflag(:,i),output(:,i),lambda(:,i),grad(:,i),hessian] = fmincon(@(c) norm(Endmember*c-Samples(i,:)'),ones(size(Endmember,2),1),A,b,Aeq,beq,zeros(1,size(Endmember,2),1),ones(size(Endmember,2),1));
    err(:,i) = sqrt(diag(inv(hessian)));
    [xl(:,i),resnorm(:,i),residual(:,i)]=lsqlin(Endmember,Samples(i,:),A,b,Aeq,beq,zeros(size(Endmember,2),1),ones(size(Endmember,2),1)); %x = lsqlin(C,d,A,b) solves the linear system C*x = d in the least-squares sense, subject to A*x ? b.
end

Isotopes=array2table(xf*100, 'VariableNames',Samplenames,'RowNames',Endnames);
%writetable(Isotopes,horzcat(importpath(1:end-9),'Deconvolution_',importpath(end-8:end)),'WriteRowNames',true)
Isotopeerr=array2table(err*100, 'VariableNames',Samplenames,'RowNames',Endnames);
Isotopeerr2=array2table(err*100, 'VariableNames',Samplenames,'RowNames',Endnames);
%Residuals=array2table(vecnorm(residual(1:8,:).*100).^2, 'VariableNames',Samplenames)


MonteSim(monterun)={Isotopes.Variables};
 end
%% Calculate Mean and error from simulations
for i=1:size(xf,2)
    for j=1:size(xf,1)
val=cellfun(@(v)v(j,i),MonteSim);
Isotopes(j,i)={mean(val)};
pr5=prctile(val,[5 95]);
pr25=prctile(val,[25 75]);
err=abs(pr5-mean(val));
err2=abs(pr25-mean(val));
Isotopeerr(j,i)={max(err)};
Isotopeerr2(j,i)={max(err2)};
    end
end
writetable(Isotopes,Savepath,'Sheet','Isotopes Absolute','WriteRowNames',true);
writetable(Isotopeerr,Savepath,'Sheet','5 95 Quantile','WriteRowNames',true);
writetable(Isotopeerr2,Savepath,'Sheet','25 75 Quantile','WriteRowNames',true);


 else

%% _______________________________________________________________________________________________________________________________________________________________
%% WITHOUT MONTE CARLO
%% ________________________________________________________________________________________________________________________________________________________________
 
 % Building endmember matrix from the specified endmembers

EndmemberXezero=[EndmemberXe.Variables;zeros(size(EndmemberNe,1)+size(EndmemberAKX,1),size(EndmemberXe,2))]; %adding zeros
EndmemberNezero=[zeros(size(EndmemberXe,1),size(EndmemberNe.Variables,2));EndmemberNe.Variables;zeros(size(EndmemberAKX,1),size(EndmemberNe.Variables,2))];
EndmemberAKXzero=[zeros(size(EndmemberXe,1)+size(EndmemberNe.Variables,1),size(EndmemberAKX,2));EndmemberAKX];
Endmember=[EndmemberXezero EndmemberNezero EndmemberAKXzero];
Endnames=[strcat(WhichXeEndmembers,'Xe') strcat(WhichNeEndmembers,'Ne') strcat(WhichAKXEndmembers,'AKX')];


% Import samples

[Samplenames,SamplesXeRel,SamplesXeRelErr, SamplesAKX, SamplesAKXErr]...
    =ImportXeSamples(importpath,RelTo,Rel,RelErr,WhichAKXEndmembers,LastRow);
[SamplesNe,SamplesNeErr]=ImportNeSamples(importpath,LastRow);
if any(size(SamplesAKX))
    Samples=[SamplesXeRel SamplesNe SamplesAKX];
else
    Samples=[SamplesXeRel SamplesNe];
end

%% COMPUTE CONSTRAINTS%%%------------------------------------------------------------------------------------------------------
% Import Element proportions
if constraints
    [EndmemberNeXe,EndmemberNeXeErr]=ImportConstraints(WhichNeEndmembers,WhichXeEndmembers);
    %Set up inequality matrix
    Names=EndmemberNeXe.Properties.VariableNames;
    Variables=EndmemberNeXe.Variables;
    VariablesErr=EndmemberNeXeErr.Variables;
    for i=1:size(EndmemberNeXe,2)
        
        Xeidx=(contains(EndmemberXe.Properties.VariableNames,Names(i)));
        Xemin=Xeidx*(Variables(i)-VariablesErr(i));
        Xemax=Xeidx*(Variables(i)+VariablesErr(i));
        
        Neidx=(contains(EndmemberNe.Properties.VariableNames,Names(i)));
        A(i+i-1,:)=[Xemin -Neidx zeros(1,size(EndmemberAKX,2))];
        A(i+i,:)=[-Xemax Neidx zeros(1,size(EndmemberAKX,2))];
    end
    b=zeros(size(A,1),1);
else
    A=[];
    b=[];
end
Aeq=[];
beq=[];

%% Minimization

for i=1:size(Samples,1)
    [xf(:,i),fval(:,i),exitflag(:,i),output(:,i),lambda(:,i),grad(:,i),hessian] = fmincon(@(c) norm(Endmember*c-Samples(i,:)'),ones(size(Endmember,2),1),A,b,Aeq,beq,zeros(1,size(Endmember,2),1),ones(size(Endmember,2),1));
    err(:,i) = sqrt(diag(inv(hessian)));
    [xl(:,i),resnorm(:,i),residual(:,i)]=lsqlin(Endmember,Samples(i,:),A,b,Aeq,beq,zeros(size(Endmember,2),1),ones(size(Endmember,2),1)); %x = lsqlin(C,d,A,b) solves the linear system C*x = d in the least-squares sense, subject to A*x ? b.
end

Isotopes=array2table(xf*100, 'VariableNames',Samplenames,'RowNames',Endnames);
%writetable(Isotopes,horzcat(importpath(1:end-9),'Deconvolution_',importpath(end-8:end)),'WriteRowNames',true)
Isotopeerr=array2table(err*100, 'VariableNames',Samplenames,'RowNames',Endnames);
%Residuals=array2table(vecnorm(residual(1:8,:).*100).^2, 'VariableNames',Samplenames)
writetable(Isotopes,Savepath,'Sheet','Isotopes Relative','WriteRowNames',true);
writetable(Isotopeerr,Savepath,'Sheet','Isotopes Relative Err','WriteRowNames',true);
end
%% validation
if plotvalidation
    
    for i=1:size(Samples,1)
        mix(:,i) =(Endmember)*xl(:,i);
        mix2(:,i) =(Endmember)*xf(:,i);
    end
    for i=1:size(SamplesXeRel,1)
        figure('Name',Samplenames{i})
        subplot(2,1,1)
        errorbar(SamplesXeRel(i,1:7),SamplesXeRelErr(i,1:7))
        hold on
        plot(mix(1:7,i),'-.')
        plot(mix2(1:7,i),'-.')
        
        xticklabels({'124','126','128','130','131','134','136'})
        labeling=sprintf('^xXe/^{132}Xe / ^xXe/^{132}Xe_%s',RelTo);
        ylabel(labeling)
        xlabel('^xXe/^{132}Xe')
        legend('measured','lsqlin','fmincon')
        title(Samplenames{i})
        subplot(2,1,2)
        errorbar(SamplesNe(i,2),SamplesNe(i,1),SamplesNeErr(i,1),SamplesNeErr(i,1),SamplesNeErr(i,2),SamplesNeErr(i,2),'.')
        hold on
        plot(mix(10,i),mix(9,i),'o')
        plot(mix2(10,i),mix2(9,i),'o')
        legend('measured','lsqlin','fmincon')
        ylabel('^{20}Ne/^{22}Ne')
        xlabel('^{21}Ne/^{22}Ne')
        title(Samplenames{i})
        set(gcf, 'Position',  [500, 0, 400, 900])
    end
    
    figure
    bar(xl*100)
    xticklabels(Endnames)
    xlabel('Endmember')
    ylabel('[%]')
end

%% Plot Endmember Proportions for each Group
s=size(EndmemberXe,2);
if plotpie
    %you can specify samples belonging to a group by specifying
    %name of the group,samplenames, no. of samples in the group, e.g.
    %Isotopepie('MIL090073',{'MIL090073 S','MIL090073 L'},Endnames,2,Isotopes,s)
    for i=1:size(Samplenames,1)
        Isotopepie(Samplenames{i},Samplenames(i),Endnames,1,Isotopes,s)
    end
end

%% Plot surface of minimizing function for two Endmembers only
if plotvalidation
    if size(WhichXeEndmembers,2)<3
        c1=linspace(0,1);
        E=Endmember(1:7,1:2);
        R=Samples(1,1:7)';
        c=[repelem(c1,1,100);repmat(c1,1,100)];
        f=E*c-R;
        [C1,C2]=meshgrid(c1,c1);
        v=log(vecnorm(f));
        z=reshape(v,100,100);
        figure
        surfc(C1,C2,z)
        xlabel(horzcat(WhichXeEndmembers{1},' [%]'))
        ylabel(horzcat(WhichXeEndmembers{2},' [%]'))
        zlabel('Log Objective function value')
    end
end

   
